var days = ['SUN','MON','TUE','WED','THU','FRI','SAT'];
var months = ['JANUARY','FEBRUARY','MARCH','APRIL','MAY','JUNE','JULY','AUGUST','SEPTEMBER','OCTOBER','NOVEMBER','DECEMBER'];

window.onload = displayClock();


function displayClock(){
    var currentDateTime = new Date();
    updateSecond(currentDateTime.getSeconds())
    updateHour(currentDateTime.getHours())
    updateMinute(currentDateTime.getMinutes())
    updateDate(currentDateTime.getDate())
    updateMonth(currentDateTime.getMonth())
    updateSeparatorDay(currentDateTime.getDay())

    setTimeout(displayClock, 1000); 
  }

function updateHour(hour) {
    if (hour < 10) {hour = "0" + hour}
    document.getElementById("hour").innerHTML = hour;
}

function updateMinute(minute) {
    if (minute < 10) {minute = "0" + minute}
    document.getElementById("minute").innerHTML = minute;
}

function updateSecond(second) {
    if (second < 10) {second = "0" + second}
    document.getElementById("second").innerHTML = second;
}

function updateDate(date) {
    if (date < 10) {date = "0" + date}
    document.getElementById("date").innerHTML = date;
}

function updateMonth(month) {
    var month = months[ month ];
    document.getElementById("month").innerHTML = month;
}


function updateSeparatorDay(day) {
    var day = days[ day ];
    document.getElementById("separator-day").innerHTML = day;
    document.getElementById(day).classList.add('today');
    //add code to handle previous day class deletion
}